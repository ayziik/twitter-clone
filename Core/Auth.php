<?php


namespace Core;


interface Auth
{
    public static function logIn(array $user);

    public static function logOut();

    public static function requireLogIn();

    public static function isLoggedIn();
}