<?php


namespace Core;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class Controller
{

    public $route;
    public $view;

    public function __construct($route)
    {
        $this->route = $route;
        $this->view = new View($route);
    }

    public function render($viewName, $params = [])
    {
        $path = ROOTHPATH . '\Views\\'.$this->route['controller'];
        $loader = new FilesystemLoader($path);
        $twig = new Environment($loader);
        echo $twig->render($viewName, $params);
    }
}