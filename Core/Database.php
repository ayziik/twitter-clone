<?php


namespace Core;

use PDO;
use PDOException;

class Database
{
    public $pdo;

    public function __construct()
    {
        $config = require $_SERVER['DOCUMENT_ROOT'] . '/app/config/db.php';
        $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
        try {
            $this->pdo = new PDO($dsn, $config['user'], $config['password']);
        } catch (PDOException $e)
        {
            var_dump($e);
        }
    }
}