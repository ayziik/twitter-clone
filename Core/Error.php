<?php


namespace Core;

use Throwable;

class Error
{
    public function registerError()
    {
        set_error_handler([$this, 'errorHandler']);
        register_shutdown_function([$this, 'fatalErrorHandler']);
        set_exception_handler([$this, 'exceptionHandler']);
    }

    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        $this->showError($errno, $errstr, $errfile, $errline);

        return true;
    }

    public function fatalErrorHandler()
    {
        if ($error = error_get_last() and $error['type'] & (E_ERROR | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR)) {
            ob_get_clean();

            $this->showError($error['type'], $error['message'], $error['file'], $error['line']);
        }
    }

    public function exceptionHandler(Throwable $exception)
    {
        $this->showError(get_class($exception), $exception->getMessage(), $exception->getFile(), $exception->getLine());

        return true;
    }

    public function showError($errno, $errstr, $errfile, $errline)
    {
        echo 'Error level or class: ' . $errno . '<hr>' . 'Error message: ' . $errstr . '<hr>' . 'File: '
            . $errfile . '<hr>' . 'line: ' . $errline . '<hr>';
    }
}