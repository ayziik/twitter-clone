<?php


namespace Core;


class View
{

    public $path;

    public function __construct($route)
    {
        $this->path = $route['controller'].'/'.$route['action'];
    }
}