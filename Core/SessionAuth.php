<?php


namespace Core;

class SessionAuth implements Auth
{

    public static function logIn(array $user)
    {
        $_SESSION['userId'] = $user['id'];
    }

    public static function logOut()
    {
        $_SESSION = [];

       self::requireLogIn();
    }

    public static function requireLogIn()
    {
        header('Location: /');
    }

    public static function isLoggedIn()
    {
        return isset($_SESSION['userId']) ;
    }
}