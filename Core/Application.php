<?php


namespace Core;


class Application
{
    public static $db;

    public static function init()
    {
        self::$db = new Database();

        (new Error())->registerError();

        $router = new Router();

        $router->run();

    }

};