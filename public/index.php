<?php

require_once '../vendor/autoload.php';

use API\API;
use Core\Application;

$request = explode('/', $_GET['q']);

if ($request[0] === 'api') {
    API::init();
} else {
    Application::init();
}