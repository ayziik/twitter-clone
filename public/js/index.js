const searchInput = document.querySelector('.header__search');
const searchResultBlock = document.querySelector('.search-result');
const toChatBtn = document.querySelector('.header__tweet-btn');
const rightSidebarBlock = document.querySelector('.right-sidebar');

// SEARCH START

function showSearchResult(searchResult) {
    searchResult.forEach(function(item) {
        let profileimg = `<img src="../../${item.profileimg}" alt="#" class="search-result__photo d-block">`;

        if  (item.profileimg === 'NULL') {
            profileimg = `<img class="search-result__photo d-block">`;
        }

        let userForSearch = `
            <div class="search-result__user d-flex">
              <div class="search-result__avatar mr-2">
                <a href="./profile?username=${item.username}">
                    ${profileimg}
                </a>
              </div>
        
              <div class="search-result__info">
                <a href="./profile?username=${item.username}" class="search-result__user-name">${item.firstname} ${item.lastname}</a>
                <a href="./profile?username=${item.username}" class="search-result__nickname">@${item.username}</a>
                <button class="search-result__btn">Following</button>
              </div>
            </div>
        `

        searchResultBlock.insertAdjacentHTML('beforeend', userForSearch);
    })
}

function showNotFound() {
    let userNotFound = `
            <div class="search-result__user">
              <p class="search-result__user-name text-center">Sorry, but we did not find anybody for this request. Please try again.</p>
              <img src="../../../public/img/sorry.jpg" alt="Sorry, but we did not find anybody for this request">
            </div>
        `

    searchResultBlock.insertAdjacentHTML('beforeend', userNotFound);
}


async function doSearchQuery(query) {
    let formData = new FormData();
    formData.append("search", query);

    let response = await fetch('../user/searchQuery', {
        method: 'POST',
        body: formData
    });

    return await response.json();
}

searchInput.addEventListener('keyup',  async function (evt) {
    evt.preventDefault();

    if (evt.keyCode === 13 && searchInput.value) {
        if (window.location.pathname !== '/user/search') {
            localStorage.setItem('searchQuery', searchInput.value);

            window.location.href = '../user/search';
        }

        let resultOfSearchQuery = await doSearchQuery(searchInput.value);

        if (resultOfSearchQuery.length === 0) {
            while (searchResultBlock.firstChild) {
                searchResultBlock.removeChild(searchResultBlock.firstChild);
            }

            showNotFound();

            return;
        }

        while (searchResultBlock.firstChild) {
            searchResultBlock.removeChild(searchResultBlock.firstChild);
        }

        showSearchResult(resultOfSearchQuery);
    }
});

document.addEventListener('DOMContentLoaded', async function (evt) {
    let searchQueryFromStorage = localStorage.getItem('searchQuery');

    if (window.location.pathname === '/user/search' && searchQueryFromStorage) {
       let resultOfSearchQuery = await doSearchQuery(searchQueryFromStorage);

        if (resultOfSearchQuery.length === 0) {
            showNotFound();

            return;
        }

       showSearchResult(resultOfSearchQuery);

       localStorage.removeItem('searchQuery');
    }
})

// SEARCH END

toChatBtn.addEventListener('click', () => {
    window.location.href = "../user/chat";
});
