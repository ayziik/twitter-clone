const like = document.querySelectorAll('.tweet_like_path');
let likesQuantity = document.querySelectorAll('.tweet__likes-quantity');


like.forEach(e => {

    e.addEventListener('click', () => {
            const url = '../post/like';

            let oReq = new XMLHttpRequest();

            oReq.open('POST', url);
            oReq.send(JSON.stringify(e.dataset.id));

            oReq.onload = function () {
                if (oReq.responseText === 'like') {
                    e.style.fill = 'red'
                } else {
                    e.style.fill = '#8899a6'
                }
                console.log('DONE', oReq.readyState); // readyState будет равно 4
            };

    })
});