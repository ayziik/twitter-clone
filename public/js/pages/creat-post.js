'use strict';

let postForm = document.querySelector('.creat-tweet');
let postIdForm = document.querySelectorAll('.post-id');
let sendPost = document.querySelector('.post-tweet');
let addFileToPost = document.querySelector('.add-file-tweet');
let delPost = document.querySelectorAll('.del-tweet');
let updatePost = document.querySelectorAll('.upd-tweet');
let tweetCounter = document.querySelector('.tweet-counter');
let postText = document.querySelector('.creat-tweet__input');
let i = 2;
let tweetHeight = postText.scrollHeight;
let postsContainer = document.querySelector('.posts-container');
let imageDropbox = document.querySelector('.picture-dropbox');
let removeUpload = document.querySelector('.remove-file');
let chooseUpload = document.querySelector('.choose-file');

let imageBase64;

postText.addEventListener('keyup', function () {
    let counter = postText.value.length;

    if (counter > 0 && counter <= 200) {
        addFileToPost.removeAttribute('disabled');
        sendPost.removeAttribute('disabled');
        tweetCounter.innerHTML = counter + "/200";
    } else if (counter > 200) {
        addFileToPost.setAttribute('disabled', 'disabled');
        sendPost.setAttribute('disabled', 'disabled');
        tweetCounter.innerHTML = "<span style='color: red'>Too much characters inputed!!!   " + counter + "/200</span>";
    } else {
        addFileToPost.setAttribute('disabled', 'disabled');
        sendPost.setAttribute('disabled', 'disabled');
        tweetCounter.innerHTML = "0/200";
    }
});

postText.addEventListener('input', function () {
    if (postText.scrollHeight >= tweetHeight + 20) {
        tweetHeight = postText.scrollHeight;
        console.log(tweetHeight);
        console.log(postText.scrollHeight);
        i++;
        postText.setAttribute('rows', i);
    }
});


sendPost.addEventListener('click', function (e) {
    let url = '../post/create';

    let oReq = new XMLHttpRequest();

    const formData = new FormData(postForm);
    if (imageBase64) {
        formData.append('post_img', imageBase64);
    }

    oReq.open('POST', url);
    oReq.send(formData);

    oReq.onload = function () {
        let newPost = JSON.parse(oReq.responseText);

        postsContainer.insertAdjacentHTML('afterbegin', newPost);
        postText.value = '';
        addFileToPost.style.backgroundColor = '#e3405f';
        console.log('DONE', oReq.readyState); // readyState будет равно 4
    };

});

for (let i=0; i<delPost.length; i++){
    delPost[i].addEventListener('click', function (e) {
        let url = '../post/delete';

        let oReq = new XMLHttpRequest();

        const formData = new FormData(postIdForm[i]);
        oReq.open('POST', url);
        oReq.send(formData);

        window.location.href = '/';
    });
}

for (let i=0; i<updatePost.length; i++){
    updatePost[i].addEventListener('click', function (e) {
        let text = prompt("Update your post!", "write your text here");
        if(text){
            let url = '../post/update';

            let oReq = new XMLHttpRequest();

            const formData = new FormData(postIdForm[i]);
            formData.append('text', text);
            oReq.open('POST', url);

            oReq.send(formData);

            window.location.href = '/';
        }
    });
}

//Modal drag-and-drop
function handleFiles(files) {
    for (let i = 0; i < files.length; i++) {
        let file = files[i];

        if (!file.type.startsWith('image/')){ continue }

        let img = document.createElement("img");
        img.classList.add("obj");
        img.file = file;
        imageDropbox.appendChild(img); // Предполагается, что "preview" это div, в котором будет отображаться содержимое.

        let reader = new FileReader();
        reader.onload = (function(aImg) {
            return function(e) {
                aImg.src = e.target.result;
                imageBase64 = e.target.result;
                console.log(imageBase64)
            };
        })(img);
        reader.readAsDataURL(file);

    }
}

function dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
}

function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
}

function drop(e) {
    e.stopPropagation();
    e.preventDefault();

    let dt = e.dataTransfer;
    let files = dt.files;

    chooseUpload.removeAttribute('disabled');
    handleFiles(files);
}



imageDropbox.addEventListener("dragenter", dragenter, false);
imageDropbox.addEventListener("dragover", dragover, false);
imageDropbox.addEventListener("drop", drop, false);

removeUpload.addEventListener('click', () => {
   imageBase64 = false;
});

chooseUpload.addEventListener('click', () => {
    addFileToPost.style.backgroundColor = '#211870';
});

