'use strict';
let formPersonInfo = document.forms['personalInfo'];
let formChangePass = document.forms['changePass'];
let aboutMeCounter = document.querySelector('.about-me-counter');
let personInfoInputs = {
    f_name: {
        elem: document.querySelector('#first_name'),
        isValid: false
    },
    l_name: {
        elem: document.querySelector('#last_name'),
        isValid: false
    },
    email: {
        elem: document.querySelector('#email'),
        isValid: false
    },
    username: {
        elem: document.querySelector('#username'),
        isValid: false
    },
    about_me: {
        elem: document.querySelector('#about-me'),
        isValid: true
    },
    birthday: {
        elem: document.querySelector('#birthdate'),
        isValid: false
    }
};
let updateBtn = document.querySelector('.upd-btn');
let changePassTable = {
    currPass: {
        elem: document.querySelector('#old_password'),
        isValid: true
    },
    pass: {
        elem: document.querySelector('#new_password'),
        isValid: false
    },
    confPass: {
        elem: document.querySelector('#conf_password'),
        isValid: false
    }
};
let changePassBtn = document.querySelector('.change-pass-btn');
let disabledUpd;
let disabledPass;

personInfoInputs.f_name.elem.addEventListener('input', function () {
    if (this.value === '') {
        this.style.borderBottomColor = '#cccccc';
        this.nextElementSibling.classList.add('d-none');
    } else {
        if (!/[A-Za-z]{3,30}/.test(this.value)) {
            this.style.borderBottomColor = 'red';
            this.nextElementSibling.classList.remove('d-none');
        } else {
            personInfoInputs.f_name.isValid = true;
            this.style.borderBottomColor = '#cccccc';
            this.nextElementSibling.classList.add('d-none');
        }
    }
});

personInfoInputs.l_name.elem.addEventListener('input', function () {
    if (this.value === '') {
        this.style.borderBottomColor = '#cccccc';
        this.nextElementSibling.classList.add('d-none');
    } else {
        if (!/[A-Za-z]{3,30}/.test(this.value)) {
            this.style.borderBottomColor = 'red';
            this.nextElementSibling.classList.remove('d-none');
        } else {
            personInfoInputs.l_name.isValid = true;
            this.style.borderBottomColor = '#cccccc';
            this.nextElementSibling.classList.add('d-none');
        }
    }
});

personInfoInputs.email.elem.addEventListener('input', function () {
    if (this.value === '') {
        this.style.borderBottomColor = '#cccccc';
        this.nextElementSibling.classList.add('d-none');
    } else {
        if (!/^[a-zA-Z0-9]+@[a-z]+\.[a-z]{2,3}$/.test(this.value)) {
            this.style.borderBottomColor = 'red';
            this.nextElementSibling.classList.remove('d-none');
        } else {
            personInfoInputs.email.isValid = true;
            this.style.borderBottomColor = '#cccccc';
            this.nextElementSibling.classList.add('d-none');
        }
    }
});

personInfoInputs.username.elem.addEventListener('input', function () {
    if (this.value === '') {
        this.style.borderBottomColor = '#cccccc';
        this.nextElementSibling.classList.add('d-none');
    } else {
        if (!/[a-z]{3,}[A-Z0-9]+[a-z]*[A-Z0-9]+/.test(this.value) || this.value.length < 5) {
            this.style.borderBottomColor = 'red';
            this.nextElementSibling.classList.remove('d-none');
        } else {
            personInfoInputs.username.isValid = true;
            this.style.borderBottomColor = '#cccccc';
            this.nextElementSibling.classList.add('d-none');
        }
    }
});

personInfoInputs.about_me.elem.addEventListener('keyup', function () {
    aboutMeCounter.innerHTML = this.value.length + '/1000';
    if(this.value.length > 1000){
        this.style.borderBottomColor = 'red';
        personInfoInputs.about_me.isValid = false;
    } else {
        this.style.borderBottomColor = '#cccccc';
        personInfoInputs.about_me.isValid = true;
    }
});

personInfoInputs.birthday.elem.addEventListener('input', function () {
    if(!/[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(this.value)){
        this.style.borderBottomColor = 'red';
    } else {
        personInfoInputs.birthday.isValid = true;
        this.style.borderBottomColor = '#cccccc';
    }
});

formPersonInfo.addEventListener('change', function () {
    for (let elem in personInfoInputs) {
        if(personInfoInputs[elem].isValid){
            disabledUpd = false;
        } else {
            disabledUpd = true;
            break;
        }
    }
    disabledUpd ? updateBtn.disabled = true : updateBtn.disabled = false;
});

changePassTable.pass.elem.addEventListener('input', function () {
    if (this.value === '') {
        this.style.borderBottomColor = '#cccccc';
        this.nextElementSibling.classList.add('d-none');
    } else {
        if (!/[a-z]{3,}[A-Z0-9]+[a-z]*[A-Z0-9]+/.test(this.value) || this.value.length < 6) {
            this.style.borderBottomColor = 'red';
            this.nextElementSibling.classList.remove('d-none');
        } else {
            console.log(this.value);
            changePassTable.pass.isValid = true;
            this.style.borderBottomColor = '#cccccc';
            this.nextElementSibling.classList.add('d-none');
        }
    }
});

changePassTable.confPass.elem.addEventListener('input', function () {
    if (this.value === '') {
        this.style.borderBottomColor = '#cccccc';
        this.nextElementSibling.classList.add('d-none');
    } else {
        if (this.value !== changePassTable.pass.elem.value) {
            this.style.borderBottomColor = 'red';
            this.nextElementSibling.classList.remove('d-none');
        } else {
            changePassTable.confPass.isValid = true;
            this.style.borderBottomColor = '#cccccc';
            this.nextElementSibling.classList.add('d-none');
        }
    }
});

formChangePass.addEventListener('change', function () {
    for (let elem in changePassTable) {
        if(changePassTable[elem].isValid){
            disabledPass = false;
        } else {
            disabledPass = true;
            break;
        }
    }

    disabledPass ? changePassBtn.disabled = true : changePassBtn.disabled = false;
});