const signUpForm = document.querySelector('.registration-form');
const firstNameInput = document.querySelector('.signup-firstname');
const lastNameInput = document.querySelector('.signup-lastname');
const emailInput = document.querySelector('.signup-email');
const usernameInput = document.querySelector('.signup-username');
const dateOfBirthInput = document.querySelector('.datepicker-here');
const passwordInput = document.querySelector('.signup-password');
const passwordConfirmation = document.querySelector('.signup-password-confirm');

function firstNameValidation(firstName) {
    let testFirstName = /^[A-Za-zА-ЯЁа-яё]{3,}$/g.test(firstName);
    let errorMessage = document.querySelector('.valid-firstname');

    if (!testFirstName) {
        errorMessage.classList.add('signup-error');
        firstNameInput.classList.add('signup-input--invalid');

        return false;
    } else {
        errorMessage.classList.remove('signup-error');
        firstNameInput.classList.remove('signup-input--invalid');

        return true;
    }
}

function lastNameValidation(lastName) {
    let testLastName = /^[A-Za-zА-ЯЁа-яё]{3,}$/g.test(lastName);
    let errorMessage = document.querySelector('.valid-lastname');

    if (!testLastName) {
        errorMessage.classList.add('signup-error');
        lastNameInput.classList.add('signup-input--invalid');

        return false;
    } else {
        errorMessage.classList.remove('signup-error');
        lastNameInput.classList.remove('signup-input--invalid');

        return true;
    }
}

function emailValidation(email) {
    let testEmail = /^([A-Za-z0-9][A-Za-z0-9]*\.?\-?_?)*[A-Za-z0-9]*@([A-Za-z0-9]+([A-Za-z0-9-]*[A-Za-z0-9]+)*\.)+[A-Za-z]{2,}$/g.test(email);
    let errorMessage = document.querySelector('.valid-email');

    if (!testEmail) {
        errorMessage.classList.add('signup-error');
        emailInput.classList.add('signup-input--invalid');

        return false;
    } else {
        errorMessage.classList.remove('signup-error');
        emailInput.classList.remove('signup-input--invalid');

        return true;
    }
}

function usernameValidation(username) {
    let testUsername = /^[A-Za-z]{3,}$/g.test(username);
    let errorMessage = document.querySelector('.valid-username');

    if (!testUsername) {
        errorMessage.classList.add('signup-error');
        usernameInput.classList.add('signup-input--invalid');

        return false;
    } else {
        errorMessage.classList.remove('signup-error');
        usernameInput.classList.remove('signup-input--invalid');

        return true;
    }
}

function dateOfBirthValidation(dateOfBirth) {
    let testLastName = /^[0-9]{2}\.[0-9]{2}\.(19|20)([0-9]{2})$/g.test(dateOfBirth);
    let errorMessage = document.querySelector('.valid-dateofbirth');

    if (!testLastName) {
        errorMessage.classList.add('signup-error');
        dateOfBirthInput.classList.add('signup-input--invalid');

        return false;
    } else {
        errorMessage.classList.remove('signup-error');
        dateOfBirthInput.classList.remove('signup-input--invalid');

        return true;
    }
}

function passwordValidation(password) {
    let testPass = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g.test(password);
    let errorMessage = document.querySelector('.valid-password');

    if (!testPass) {
        errorMessage.classList.add('signup-error');
        passwordInput.classList.add('signup-input--invalid');

        return false;
    } else {
        errorMessage.classList.remove('signup-error');
        passwordInput.classList.remove('signup-input--invalid');

        return true;
    }
}

function checkPasswordMatch() {
    let errorMessage = document.querySelector('.valid-confirm-password');

    if (passwordInput.value !== passwordConfirmation.value) {
        errorMessage.classList.add('signup-error');
        passwordConfirmation.classList.add('signup-input--invalid');

        return false;
    } else {
        errorMessage.classList.remove('signup-error');
        passwordConfirmation.classList.remove('signup-input--invalid');

        return true;
    }
}

signUpForm.addEventListener('submit', async function (evt) {
    evt.preventDefault();

    let isFirstNameValid = firstNameValidation(firstNameInput.value);

    let isLastNameValid = lastNameValidation(lastNameInput.value);

    let isEmailValid = emailValidation(emailInput.value);

    let isUsernameValid = usernameValidation(usernameInput.value);

    let isDateOfBirthValid = dateOfBirthValidation(dateOfBirthInput.value);

    let isPasswordValid = passwordValidation(passwordInput.value);

    let isPasswordMatch = checkPasswordMatch();

    if (isPasswordValid && isPasswordMatch && isFirstNameValid
        && isLastNameValid && isEmailValid && isUsernameValid && isDateOfBirthValid) {

        signUpForm.submit();

    }
});