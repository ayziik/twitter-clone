const tweetsTab = document.querySelector('.tabs__tweets');
const followingTab = document.querySelector('.tabs__following');
const followersTab = document.querySelector('.tabs__followers');
const sectionWithTweets = document.querySelector('.tweets-tab');
const sectionWithFollowing = document.querySelector('.following-tab');
const sectionWithFollowers = document.querySelector('.followers-tab');

const uploadAvatarForm = document.querySelector('.upload-avatar');
const uploadAvatarInput = document.querySelector('.upload-avatar__input');
const uploadAvatarSubmit = document.querySelector('.upload-avatar__submit');
const uploadAvatarButtonText = document.querySelector('.upload-avatar__label-button-text');

tweetsTab.addEventListener('click', function () {
    if (!tweetsTab.classList.contains('tabs__info--active')) {
        tweetsTab.classList.add('tabs__info--active');
        followingTab.classList.remove('tabs__info--active');
        followersTab.classList.remove('tabs__info--active');
    }

    if (!sectionWithTweets.classList.contains('active-tab')) {
        sectionWithTweets.classList.add('active-tab');
        sectionWithFollowing.classList.remove('active-tab');
        sectionWithFollowers.classList.remove('active-tab');
    }
});

followingTab.addEventListener('click', function () {
    if (!followingTab.classList.contains('tabs__info--active')) {
        followingTab.classList.add('tabs__info--active');
        tweetsTab.classList.remove('tabs__info--active');
        followersTab.classList.remove('tabs__info--active');
    }

    if (!sectionWithFollowing.classList.contains('active-tab')) {
        sectionWithFollowing.classList.add('active-tab');
        sectionWithTweets.classList.remove('active-tab');
        sectionWithFollowers.classList.remove('active-tab');
    }
});

followersTab.addEventListener('click', function () {
    if (!followersTab.classList.contains('tabs__info--active')) {
        followersTab.classList.add('tabs__info--active');
        followingTab.classList.remove('tabs__info--active');
        tweetsTab.classList.remove('tabs__info--active');
    }

    if (!sectionWithFollowers.classList.contains('active-tab')) {
        sectionWithFollowers.classList.add('active-tab');
        sectionWithTweets.classList.remove('active-tab');
        sectionWithFollowing.classList.remove('active-tab');
    }
});

// UPLOAD AVATAR START

uploadAvatarForm.addEventListener('submit', async function(evt) {
    evt.preventDefault();

    let formData =  new FormData();
    let avatarPhoto = uploadAvatarInput.files[0];

    formData.append('avatar', avatarPhoto);

    let response = await fetch('../user/uploadAvatar', {
        method: 'POST',
        body: formData
    });

    let result = await response.json();

    if (result.isAvatarUpdate) {
        window.location.href = '../user/profile';
    } else {
        alert('Error');
    }
})

uploadAvatarInput.addEventListener('change', function(evt) {
    evt.preventDefault();

    if (uploadAvatarInput.files.length > 0) {
        uploadAvatarSubmit.classList.remove('visually-hidden');

        uploadAvatarButtonText.textContent = uploadAvatarInput.files[0].name;
    }
})

// UPLOAD AVATAR END
// UPLOAD AVATAR END
