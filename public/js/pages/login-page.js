const loginForm = document.querySelector('.login-form');
const loginInput = document.querySelector('.login-input');
const passwordInput = document.querySelector('.password');
const submitBtn = document.querySelector('.submit-login');
const loginErr = document.querySelector('.invalid-login');
const passErr = document.querySelector('.password-helper');

function loginCheck(login) {
    const mailRegex = /^([A-Za-z0-9][A-Za-z0-9]*\.?\-?_?)*[A-Za-z0-9]*@([A-Za-z0-9]+([A-Za-z0-9-]*[A-Za-z0-9]+)*\.)+[A-Za-z]{2,}$/g;
    const usernameRegex = /^[A-Za-z]{3,}$/g;

    let isEmailOrUsername = login.search('@');

    if (isEmailOrUsername >= 0) {
        if(!mailRegex.test(login)){
            loginErr.classList.remove('d-none');
            loginInput.style.borderBottomColor = '#f44336';
            return false;
        }else{
            loginErr.classList.add('d-none');
            loginInput.style.borderBottomColor = '#d1d1d1';
            return true;
        }
    }

    if (isEmailOrUsername === -1) {
        if(!usernameRegex.test(login)){
            loginErr.classList.remove('d-none');
            loginInput.style.borderBottomColor = '#f44336';
            return false;
        }else{
            loginErr.classList.add('d-none');
            loginInput.style.borderBottomColor = '#d1d1d1';
            return true;
        }
    }
}

function passwordCheck(password) {
    let testPass = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g.test(password);

    if(!testPass){
        passErr.classList.remove('d-none');
        passwordInput.style.borderBottomColor = '#f44336';
        return false;
    }else{
        passErr.classList.add('d-none');
        passwordInput.style.borderBottomColor = '#d1d1d1';
        return true;
    }
}

loginForm.addEventListener('change', function () {
    if (loginInput.value === '') {
        loginErr.classList.add('d-none');
        loginInput.style.borderBottomColor = '#d1d1d1';
    }

    if (passwordInput.value === '') {
        passErr.classList.add('d-none');
        passwordInput.style.borderBottomColor = '#d1d1d1';
    }
});

loginForm.addEventListener('submit', e =>{
    e.preventDefault();

    let isPasswordValid = passwordCheck(passwordInput.value);
    let isLoginValid = loginCheck(loginInput.value);

    if(isLoginValid && isPasswordValid) {
        loginForm.submit();
    }
});


