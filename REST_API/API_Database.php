<?php
namespace API;

use PDO;
use PDOException;

class API_Database
{
    public $pdo;

    public function __construct()
    {
        $config = require $_SERVER['DOCUMENT_ROOT'] . '/REST_API/config/api_db.php';
        $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
        try {
            $this->pdo = new PDO($dsn, $config['user'], $config['password']);
        } catch (PDOException $e)
        {
            var_dump($e);
        }
    }
}