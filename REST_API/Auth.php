<?php


namespace API;


use API\helpers\Headers;
use Firebase\JWT\JWT;
use PDO;

class Auth
{
    static function auth($id) {
        $key = "secret_key";
        $payload = array(
            "iss" => "http://devcommunication.ua",
            "userId" => $id
        );

        return JWT::encode($payload, $key);
    }

    static function checkAuth() {

        Headers::headers();

        $token = file_get_contents('php://input');

        try {
            $decoded = JWT::decode($token, "secret_key", array('HS256'));
        } catch (\UnexpectedValueException $unexpectedValueException) {
            $res = [
                'status' => 'Failed',
                'error' => $unexpectedValueException->getMessage()
            ];

            echo json_encode($res);
            exit();
        }


        $decoded_array = (array) $decoded;

        if ($decoded_array['iss'] === 'http://devcommunication.ua') {
            $db = new API_Database;

            $stmt_user = $db->pdo->prepare('SELECT * FROM users WHERE id = :userId');
            $stmt_user->bindValue(':userId', $decoded_array['userId']);
            $stmt_user->execute();

            $stmt_subscriptions = $db->pdo->prepare('SELECT COUNT(id) as subscriptionsCount FROM subscriptions WHERE follower_id = :userId');
            $stmt_subscriptions->bindValue(':userId', $decoded_array['userId']);
            $stmt_subscriptions->execute();

            $stmt_posts = $db->pdo->prepare('SELECT COUNT(id) as postsCount FROM posts WHERE user_id = :userId');
            $stmt_posts->bindValue(':userId', $decoded_array['userId']);
            $stmt_posts->execute();

            $user = $stmt_user->fetch(PDO::FETCH_ASSOC);
            $subscriptions = $stmt_subscriptions->fetch(PDO::FETCH_ASSOC);
            $posts = $stmt_posts->fetch(PDO::FETCH_ASSOC);

            $user['subscriptionsCount'] = intval($subscriptions['subscriptionsCount']);
            $user['postsCount'] = intval($posts['postsCount']);

            $res = [
                'user' => $user,
                'status' => 'Success',
            ];

            echo json_encode($res);
        } else {
            $res = [
                'status' => 'Failed',
                'error' => 'Invalid token'
            ];

            echo json_encode($res);
        }
    }
}