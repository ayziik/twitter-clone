<?php


namespace API\helpers;


class Headers
{
    public static function headers() {
        // необходимые HTTP-заголовки
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: *");
        header("Access-Control-Allow-Credentials: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json; charset=UTF-8");
    }
}