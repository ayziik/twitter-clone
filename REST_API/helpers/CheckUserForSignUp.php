<?php


namespace API\helpers;


use API\API_Database;
use PDO;

class CheckUserForSignUp
{
    public static function getUser(string $email, string $username)
    {

        $db = new API_Database;

        $IsUserExist = $db->pdo->prepare('SELECT * FROM users WHERE email = :email OR username = :username LIMIT 1');
        $IsUserExist->bindValue(':email', $email);
        $IsUserExist->bindValue(':username', $username);
        $IsUserExist->execute();

        $result = $IsUserExist->fetch(PDO::FETCH_ASSOC);
        $errors = ['errors' => []];

        if ($result['email'] === $email) {
            $errors['errors']['emailError'] = 'User with this email already exist';
        }

        if ($result['username'] === $username) {
            $errors['errors']['usernameError'] = 'User with this username already exist';
        }

        if ($errors['errors']) {
            http_response_code(200);

            echo json_encode($errors);

            return true;
        }

        return false;
    }
}