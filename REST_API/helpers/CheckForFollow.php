<?php


namespace API\helpers;


use API\API_Database;
use PDO;

class CheckForFollow
{
    public static function checkForFollow(array $data)
    {
        $db = new API_Database;

        $IsUserExist = $db->pdo->prepare('SELECT * FROM subscriptions WHERE follower_id = :follower 
                                                    AND following_id = :following LIMIT 1');
        $IsUserExist->bindValue(':follower', $data['followerID']);
        $IsUserExist->bindValue(':following', $data['followingID']);
        $IsUserExist->execute();

        $result = $IsUserExist->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            http_response_code(200);

            $error = ['error' => 'Already follow'];

            echo json_encode($error);

            return true;
        }

        return false;
    }
}