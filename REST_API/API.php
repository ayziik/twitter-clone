<?php
namespace API;

use API\like\Like;
use API\user\Login;
use API\user\Logout;
use API\user\SignUp;
use API\user\ReadUser;
use API\user\Search;

use API\post\Post;
use API\user\Subscriptions;
use API\user\Update;

class API
{
    public static function init() {
        $request = explode('/', $_GET['q']);

        $type = $request[1];
        $whatToDo = $request[2];
        $id = $request[3];

        if ($type === 'user') {
            switch ($whatToDo) {
                case 'signup':
                    SignUp::addToDatabase();
                    break;
                case 'login':
                    Login::login();
                    break;
                case 'auth-check':
                    Auth::checkAuth();
                    break;
                case 'logout':
                    Logout::logout();
                    break;
                case 'read':
                    if ($id) {
                        ReadUser::readOneUser($id);
                        break;
                    } else {
                        ReadUser::readAllUsers();
                        break;
                    }
                case 'search':
                    Search::findUser($id);
                    break;
                case 'update-avatar':
                    Update::updateAvatar();
                    break;
                case 'update-info':
                    Update::updateInfo();
                    break;
                case 'change-pass':
                    Update::changePassword();
                    break;
                case 'follow':
                    Subscriptions::follow();
                    break;
                case 'unfollow':
                    Subscriptions::unFollow();
                    break;
                case 'get-all-following':
                    Subscriptions::getAllFollowing($id);
                    break;
                case 'get-all-followers':
                    Subscriptions::getAllFollowers($id);
                    break;
            }
        }

        if ($type === 'post') {
            switch ($whatToDo) {
                case 'create':
                    Post::addToDatabase();
                    break;
                case 'read-all':
                    Post::getAllPosts($id);
                    break;
                case 'read-one':
                    Post::getAllPostsByUserSession($id);
                    break;
                case 'update':
                    Post::updatePost();
                    break;
                case 'delete':
                    Post::deletePost($id);
                    break;
            }
        }

        if ($type === 'like') {
            $user_id = $_GET['user_id'];
            $post_id = $_GET['post_id'];
            switch ($whatToDo) {
                case 'create':
                    Like::addToDatabase($user_id, $post_id);
                    break;
                case 'delete':
                    Like::deleteLike($user_id, $post_id);
                    break;
                case 'check':
                    Like::isLike($user_id, $post_id);
                    break;
                case 'read':
                    Like::getTotalLikes($post_id);
                    break;
            }
        }
    }
}
