<?php
namespace API\like;

use API\API_Database;
use API\helpers\Headers;
use PDO;

class Like
{
    public static function addToDatabase($user_id, $post_id) {
        // необходимые HTTP-заголовки
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("INSERT INTO likes (user_id, post_id)
                                                VALUES (:user_id, :post_id)");
        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':post_id', $post_id);

        if ($stmt->execute()) {
            http_response_code(201);

            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    public static function deleteLike($user_id, $post_id) {
        // необходимые HTTP-заголовки
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("DELETE FROM likes WHERE user_id = :user_id AND post_id = :post_id");
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':post_id', $post_id);

        if ($stmt->execute()) {
            http_response_code(201);

            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    public static function isLike($user_id, $post_id) {
        // необходимые HTTP-заголовки
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("SELECT user_id, post_id FROM likes
                                                WHERE user_id = :user_id AND post_id = :post_id");
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':post_id', $post_id);

        $stmt->execute();
        $res = $stmt->fetch();

        if ($stmt->execute()) {
            http_response_code(200);

            echo json_encode(boolval($res));
        } else {
            echo json_encode(boolval($res));
        }
    }

    public static function getTotalLikes($post_id)
    {
        // необходимые HTTP-заголовки
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("SELECT COUNT(post_id) FROM likes
                                                WHERE post_id = :post_id");
        $stmt->bindParam(':post_id', $post_id);

        $stmt->execute();
        $likes = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->execute()) {
            http_response_code(200);

            echo json_encode($likes["COUNT(post_id)"]);
        } else {
            echo json_encode(false);
        }
    }
}