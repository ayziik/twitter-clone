<?php


namespace API\post;

use API\API_Database;
use API\helpers\Headers;
use Core\Database;
use PDO;

class Post
{
    public static function addToDatabase() {
        // необходимые HTTP-заголовки
        Headers::headers();

        /*$data=json_decode(file_get_contents('php://input'),1);*/

        $path = '../resources/posts/';
        $fname = $_FILES['postImg']['name'];

        /*$data = [
            'user_id' => $data['userId'],
            'body' => $data['text'],
        ];*/

        $db = new API_Database;
        //query to DB
        if ($_FILES)
        {
            if(move_uploaded_file($_FILES['postImg']['tmp_name'], $path .  $_POST['userId'] . $fname))
            {
                $pathForDB = 'resources/posts/' . $_POST['userId'] . $fname;

                $stmt = $db->pdo->prepare("INSERT INTO posts (user_id, body, post_img)
                                                    VALUES (:user_id, :body, :post_img)");
                $stmt->bindValue(':user_id', $_POST['userId']);
                $stmt->bindValue(':body', $_POST['postText']);
                $stmt->bindValue(':post_img', $pathForDB);
            } else {
                echo json_encode(['error' => 'Uploading error']);
                exit();
            }
        } else {
            $stmt = $db->pdo->prepare("INSERT INTO posts (user_id, body)
                                                VALUES (:user_id, :body)");
            $stmt->bindValue(':user_id', $_POST['userId']);
            $stmt->bindValue(':body', $_POST['postText']);
        }

        if ($stmt->execute()) {
            http_response_code(201);

            self::getAllPosts($_POST['userId']);
        } else {
            $res = [
                'error' => 'DB error'
            ];

            echo json_encode($res);
        }
    }

    public static function getAllPosts($user_id) {
        // необходимые HTTP-заголовки
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("SELECT posts.*, firstname, lastname, username, profileimg, COUNT(likes.post_id) as likes 
                                             FROM `posts` LEFT JOIN `users` ON posts.user_id = users.id 
                                             LEFT JOIN likes ON posts.id = likes.post_id 
                                             GROUP BY posts.id 
                                             ORDER By posts.created_at DESC");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $db->pdo->prepare("SELECT post_id FROM likes WHERE user_id = :user_id");
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();

        $likes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $key => $val) {
            $result[$key]['likes'] = intval($result[$key]['likes']);
            foreach ($likes as $likeKey => $likeVal) {
                if ( $val['id'] === $likeVal['post_id']) {
                   $result[$key]['isLike'] = true;
                   break;
                } else {
                   $result[$key]['isLike'] = false;
                }
            }
        }

        if ($result) {
            http_response_code(200);

            echo json_encode($result);
        } else {
            $res = [
                'status' => false
            ];

            echo json_encode($res);
        }
    }

    public static function getAllPostsByUserSession($id)
    {
        // необходимые HTTP-заголовки
        Headers::headers();

        $db = new API_Database;

        $stmt = $db->pdo->prepare("SELECT posts.user_id, body, posts.created_at, posts.post_img, users.firstname, users.lastname, users.username 
                                        FROM posts
                                        JOIN users ON posts.user_id = users.id
                                        WHERE users.id = :usersId ORDER BY created_at DESC");
        $stmt->bindParam(':usersId', $id);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            http_response_code(200);

            echo json_encode($result);
        } else {
            $res = [
                'status' => false
            ];

            echo json_encode($res);
        }
    }

    public static function updatePost()
    {
        // необходимые HTTP-заголовки
        Headers::headers();

        $updData=json_decode(file_get_contents('php://input'),1);

        $db = new Database();

        $stmt = $db->pdo->prepare("UPDATE posts SET body = :text WHERE id = :id");
        $stmt->bindValue(':text', $updData['postText']);
        $stmt->bindValue(':id', $updData['postId']);

        if ($stmt->execute())
        {
            self::getAllPosts($updData['userId']);
        } else {
            json_encode(['error' => 'DB error!']);
        }
    }

    public static function deletePost($postId)
    {
        // необходимые HTTP-заголовки
        Headers::headers();
        $db = new Database();

        $stmt = $db->pdo->prepare("DELETE FROM likes WHERE post_id = :id");
        $stmt->bindValue(':id', $postId);
        $stmt->execute();

        $stmt = $db->pdo->prepare("DELETE FROM posts WHERE id = :id");
        $stmt->bindValue(':id', $postId);

        if ($stmt->execute())
        {
            echo json_encode(['status' => 'Success!']);
        } else {
            echo json_encode(['error' => 'DB error!']);
        }
    }
}