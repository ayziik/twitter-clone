<?php


namespace API\user;


use API\API_Database;
use API\helpers\Headers;
use PDO;

class Search
{
    public static function findUser(string $searchQuery)
    {
        Headers::headers();

        $search = explode(' ', $searchQuery);
        $count = count($search);
        $arrayForQuery = [];
        $i = 0;

        foreach ($search as $value) {
            $i++;

            if ($i < $count) {
                $arrayForQuery[] = "CONCAT (`firstname`, `lastname`, `username`) LIKE '%" . $value . "%' OR ";
            } else {
                $arrayForQuery[] = "CONCAT (`firstname`, `lastname`, `username`) LIKE '%" . $value . "%'";
            }
        }

        $db = new API_Database;

        $pdoQuery = "SELECT * FROM users WHERE " . implode("", $arrayForQuery);

        $IsUserExist = $db->pdo->query($pdoQuery);

        $result = $IsUserExist->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            http_response_code(200);

            $response = [
                'searchResult' => $result
            ];

            echo json_encode($response);
        } else {
            $result = [
                'searchResult' => false
            ];

            echo json_encode($result);
        }
    }
}