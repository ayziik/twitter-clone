<?php


namespace API\user;


use API\API_Database;
use API\helpers\Headers;
use API\helpers\Validation;

class Update
{
    public static function updateAvatar()
    {
        Headers::headers();

        $path = '../resources/avatars/';
        $fname = $_FILES['avatar']['name'];

        if (move_uploaded_file($_FILES['avatar']['tmp_name'], $path .  $_POST['userId'] . $fname)) {
            $pathForDB = 'resources/avatars/' . $_POST['userId'] . $fname;

            $db = new API_Database;
            //query to DB
            $stmt = $db->pdo->prepare('UPDATE users SET profileimg = :data  WHERE id = :userId');
            $stmt->bindValue(':userId', $_POST['userId']);
            $stmt->bindValue(':data', $pathForDB);
            $isUpdateSuccess = $stmt->execute();

            if ($isUpdateSuccess) {
                ReadUser::readOneUser($_POST['userId']);
            }else {
                http_response_code(200);

                $result = [
                    'error' => 'DB error'
                ];

                echo json_encode($result);
            }

        } else {
            http_response_code(200);

            $result = [
                'error' => 'Server error'
            ];

            echo json_encode($result);
        }
    }

    public static function updateInfo()
    {
        Headers::headers();

        $data=json_decode(file_get_contents('php://input'),1);

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare('UPDATE users SET firstname = :firstname, lastname = :lastname, email = :email, username = :username, dob = :dob, bio = :bio  WHERE id = :userId');
        $stmt->bindValue(':userId', $data['id']);
        $stmt->bindValue(':firstname', $data['firstname']);
        $stmt->bindValue(':lastname', $data['lastname']);
        $stmt->bindValue(':email', $data['email']);
        $stmt->bindValue(':username', $data['username']);
        $stmt->bindValue(':dob', $data['dob']);
        $stmt->bindValue(':bio', $data['bio']);

        $exec = $stmt->execute();

        if ($exec) {
            http_response_code(201);

            echo json_encode($data);
        }else {
            http_response_code(200);

            $result = [
                'error' => 'DB error'
            ];

            echo json_encode($result);
        }
    }

    public static function changePassword()
    {
        Headers::headers();

        $passData=json_decode(file_get_contents('php://input'),1);

        $db = new API_Database;

        $pass_verify =false;

        if (Validation::passwordValidation($passData['currentPassword']))
        {
            $stmt = $db->pdo->prepare('SELECT password FROM users WHERE id = :userId');
            $stmt->bindValue(':userId', $passData['userId']);
            $stmt->execute();
            $pass_fetch = $stmt->fetch(\PDO::FETCH_ASSOC)['password'];


            if (password_verify($passData['currentPassword'], $pass_fetch)) {
                $pass_verify = true;
            } else {
                echo json_encode(['error' => 'Password does not match']);
            }
        } else {
            echo json_encode(['error' => 'Password invalid']);
        }

        if ($pass_verify)
        {
            $new_password = password_hash($passData['newPassword'], PASSWORD_DEFAULT);

            $stmt = $db->pdo->prepare('UPDATE users SET password = :password WHERE id = :userId');
            $stmt->bindValue(':userId', $passData['userId']);
            $stmt->bindValue(':password', $new_password);
            $result = $stmt->execute();

            if ($result)
            {
                echo json_encode(['status' => 'Success!']);
            } else {
                echo json_encode(['error' => 'DB error']);
            }
        }
    }
}