<?php


namespace API\user;


use API\API_Database;
use API\helpers\CheckForFollow;
use API\helpers\Headers;
use PDO;

class Subscriptions
{
    public static function follow()
    {
        Headers::headers();

        $query = json_decode(file_get_contents('php://input'), 1);

        $data = [
            'followerID' => $query['followerID'],
            'followingID' => $query['followingID'],
        ];

        if (!CheckForFollow::checkForFollow($data)) {
            $db = new API_Database;
            //query to DB
            $stmt = $db->pdo->prepare("INSERT INTO subscriptions (follower_id, following_id)
                                            VALUES (:followerID, :followingID)");
            $stmt->bindValue(':followerID', $data['followerID']);
            $stmt->bindValue(':followingID', $data['followingID']);

            if ($stmt->execute()) {
                self::getAllFollowing($data['followerID']);
            } else {
                $result = [
                    'error' => 'DB error'
                ];

                echo json_encode($result);
            }
        }
    }

    public static function unFollow()
    {
        Headers::headers();

       $query = json_decode(file_get_contents('php://input'), 1);

        $data = [
            'followerID' => $query['followerID'],
            'followingID' => $query['followingID'],
        ];

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("DELETE FROM subscriptions
                                            WHERE subscriptions.follower_id = :followerID 
                                            AND subscriptions.following_id = :followingID");
        $stmt->bindValue(':followerID', $data['followerID']);
        $stmt->bindValue(':followingID', $data['followingID']);

        if ($stmt->execute()) {
            self::getAllFollowing($data['followerID']);
        } else {
            $result = [
                'error' => 'DB error'
            ];

            echo json_encode($result);
        }
    }

    public static function getAllFollowing($id)
    {
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("SELECT users.* 
                                            FROM users
                                            JOIN subscriptions ON users.id = subscriptions.following_id
                                            WHERE subscriptions.follower_id = :id ORDER BY subscriptions.created_at DESC");
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            http_response_code(200);

            $response = [
                'following' => $result
            ];

            echo json_encode($response);
        } else {
            $result = [
                'status' => false
            ];

            echo json_encode($result);
        }
    }

    public static function getAllFollowers($id)
    {
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("SELECT users.* 
                                            FROM users
                                            JOIN subscriptions ON users.id = subscriptions.follower_id
                                            WHERE subscriptions.following_id = :id ORDER BY subscriptions.created_at DESC");
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            http_response_code(200);

            $response = [
                'followers' => $result
            ];

            echo json_encode($response);
        } else {
            $result = [
                'followers' => false
            ];

            echo json_encode($result);
        }
    }
}