<?php


namespace API\user;

use API\Auth;
use API\API_Database;
use API\helpers\Headers;
use API\helpers\Validation;
use PDO;

class Login
{
    public static function login()
    {

        Headers::headers();

        $db = new API_Database;
        $data=json_decode(file_get_contents('php://input'),1);

        $data = [
            'userLogin' => $data['userLogin'],
            'password' => $data['userPassword']
        ];

        if (Validation::loginValidation($data)) {

            $IsUserExist = $db->pdo->prepare('SELECT * FROM users WHERE email = :userLogin OR username = :userLogin');
            $IsUserExist->bindValue(':userLogin', $data['userLogin']);
            $IsUserExist->execute();

            $rowFromBase = $IsUserExist->fetch(PDO::FETCH_ASSOC);

            if ($rowFromBase) {
                if (password_verify($data['password'], $rowFromBase['password'])) {
                    http_response_code(201);

                    session_start();
                    $_SESSION['userId'] = $rowFromBase['id'];

                    $token = Auth::auth($rowFromBase['id']);

                    header("Authorization: JWT " . $token);

                    $rowFromBase['token'] = $token;

                    echo json_encode($rowFromBase);
                } else {
                    $result = [
                        'error' => 'Incorrect password'
                    ];
                    echo json_encode($result);
                }
            } else {
                $result = [
                    'error' => 'User not found'
                ];
                echo json_encode($result);
            }

        } else {
            $result = [
                'error' => 'Validation error'
            ];
            echo json_encode($result);
        }
    }
}