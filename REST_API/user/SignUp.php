<?php
namespace API\user;

use API\API_Database;
use API\helpers\CheckUserForSignUp;
use API\helpers\Headers;
use API\helpers\Validation;


class SignUp
{
    public static function addToDatabase()
    {
        Headers::headers();

        $query = json_decode(file_get_contents('php://input'), 1);

        $data = [
            'firstName' => $query['firstName'],
            'lastName' => $query['lastName'],
            'email' => $query['email'],
            'username' => $query['username'],
            'dateOfBirth' => $query['dateOfBirth'],
            'password' => $query['userPassword'],
        ];

        if (Validation::sinupValidation($data)) {

            if (!CheckUserForSignUp::getUser($data['email'], $data['username'])) {
                $data['password'] = password_hash( $data['password'], PASSWORD_DEFAULT);

                $db = new API_Database;
                //query to DB
                $stmt = $db->pdo->prepare("INSERT INTO users (firstname, lastname, email, username, dob, password)
                                            VALUES (:firstName, :lastName, :email, :username, :dateOfBirth, :password)");
                $stmt->bindValue(':firstName', $data['firstName']);
                $stmt->bindValue(':lastName', $data['lastName']);
                $stmt->bindValue(':email', $data['email']);
                $stmt->bindValue(':username', $data['username']);
                $stmt->bindValue(':dateOfBirth', $data['dateOfBirth']);
                $stmt->bindValue(':password', $data['password']);

                if ($stmt->execute()) {
                    http_response_code(201);

                    $result = [
                        'status' => true
                    ];

                    echo json_encode($result);
                } else {
                    $result = [
                        'error' => 'DB error'
                    ];

                    echo json_encode($result);
                }
            }
        } else {
            $result = [
                'error' => 'Validation error'
            ];
            echo json_encode($result);
        }
    }
}


