<?php


namespace API\user;


use API\API_Database;
use API\helpers\Headers;
use PDO;

class ReadUser
{
    public static function readAllUsers()
    {
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("SELECT * FROM users");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            http_response_code(200);

            $response = [
                'allUsers' => $result
            ];

            echo json_encode($response);
        } else {
            $result = [
                'error' => 'DB error'
            ];

            echo json_encode($result);
        }
    }

    public static function readOneUser($user)
    {
        Headers::headers();

        $db = new API_Database;
        //query to DB
        $stmt = $db->pdo->prepare("SELECT * FROM users WHERE id = :user OR username = :user");
        $stmt->bindValue(':user', $user);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            http_response_code(200);

            echo json_encode($result);
        } else {
            $result = [
                'status' => false
            ];

            echo json_encode($result);
        }
    }
}