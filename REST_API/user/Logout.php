<?php


namespace API\user;


use API\helpers\Headers;

class Logout
{
    static function logout() {
        Headers::headers();

        session_start();

        $_SESSION= array();
        session_destroy();

        $res = [
            'status' => true,
        ];

        echo json_encode($res);

    }
}