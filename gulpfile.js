const del = require('del');
const gulp = require('gulp');
const debug = require('gulp-debug');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const sass = require('gulp-sass');


const SRC = './src';
const PUBLIC = './public';
const paths = {
    public: {
        root: `${PUBLIC}/css`,
        css: `${PUBLIC}/css/styles.css`,
    },
    src: {
        scss: `${SRC}/scss/*.scss`,
    },
    compileWatch: {
        scss: `${SRC}/scss/**/*.scss`,
    },
    reloadWatch: {
        css: `${PUBLIC}/**/*.css`,
    }
};

const styles = (paths, outputFilename, outputPath) => {
    return gulp
      .src(paths)
      .pipe(sass().on('error', sass.logError))
      .pipe(debug({ title: 'scss:' }))
      .pipe(concat(outputFilename))
      .pipe(gulp.dest(outputPath));
};

gulp.task('clean', () => del([paths.public.css], { dot: true }));

gulp.task('styles', callback => {
    styles([paths.src.scss], 'styles.css', paths.public.root);
    callback();
});


gulp.task(
  'build',
  gulp.series(
    'clean',
    'styles'
  )
);

gulp.task('watch', () => {
    gulp.watch(paths.compileWatch.scss, gulp.series('styles'));
});

gulp.task('serve', () => {
    browserSync.init({
        logPrefix: 'WSK',
        proxy: {
            target: "twitter-clone", // TODO ЗАМЕНИТЬ на свой адрес
            ws: true
        },
    });

    browserSync.watch(paths.reloadWatch.css).on('change', reload);
});

gulp.task('default', gulp.series('build', gulp.parallel('watch', 'serve')));