<?php

return [
    '' => [
        'controller' => 'page',
        'action' => 'index'
    ],

    'about' => [
        'controller' => 'page',
        'action' => 'about'
    ],

    'user/registration' => [
        'controller' => 'user',
        'action' => 'registration'
    ],

    'user/login' => [
        'controller' => 'user',
        'action' => 'login'
    ],

    'user/logout' => [
        'controller' => 'user',
        'action' => 'logout'
    ],

    'user/home' => [
        'controller' => 'user',
        'action' => 'home'
    ],

    'user/profile' => [
        'controller' => 'user',
        'action' => 'profile'
    ],

    'user/edit' => [
        'controller' => 'user',
        'action' => 'edit'
    ],

    'user/chat' => [
        'controller' => 'user',
        'action' => 'chat'
    ],

    'user/search' => [
        'controller' => 'user',
        'action' => 'search'
    ],

    'user/searchQuery' => [
        'controller' => 'user',
        'action' => 'searchQuery'
    ],

    'user/uploadAvatar' => [
        'controller' => 'user',
        'action' => 'uploadAvatar'
    ],

    'post/create' => [
        'controller' => 'post',
        'action' => 'create'
    ],

    'post/delete' => [
        'controller' => 'post',
        'action' => 'delete'
    ],

    'post/update' => [
        'controller' => 'post',
        'action' => 'update'
    ]
];
