<?php



namespace App\Controllers;

use App\DAL\PostDAO;
use App\DAL\UserDAO;
use App\Entity\User;
use App\helpers\Encoder;
use App\helpers\Validation;
use Core\Controller;
use Core\SessionAuth;

class UserController extends Controller
{

    private $userDao;
    private $postDao;

    public function __construct($route)
    {
        parent::__construct($route);
        session_start();

        $this->userDao = new UserDAO();
        $this->postDao = new PostDAO();
    }

    public function loginAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $data = [
                'userLogin' => $_POST['userLogin'],
                'password' => $_POST['password']
            ];

            if (Validation::loginValidation($data)) {
                $user = new User();

                $checkUserForLogin = $this->userDao->checkUserForLogin($user->createUserFromArray($data));

                if ($checkUserForLogin) {
                    SessionAuth::logIn($checkUserForLogin);

                    if (SessionAuth::isLoggedIn()) {
                        header('Location: /user/home');
                    } else {
                        $error = 'Something went wrong...';
                        $this->render('login.twig', ['warning' => $error]);
                        unset ($error);
                    }
                } else {
                    $error = 'User with this email or username does not exist';
                    $this->render('login.twig', ['warning' => $error]);
                    unset ($error);
                }
            } else {
                $error = 'The entered data is invalid';
                $this->render('login.twig', ['warning' => $error]);
                unset ($error);
            }
        } else
        {
            if (SessionAuth::isLoggedIn()) {
                header('Location: home');
            } else {
                $this->render('login.twig');
            }
        }
    }

    public function registrationAction()
    {
        if (!empty($_POST)) {
            $data = [
                'firstName' => $_POST['firstName'],
                'lastName' => $_POST['lastName'],
                'email' => $_POST['email'],
                'username' => $_POST['username'],
                'dateOfBirth' => $_POST['dateOfBirth'],
                'password' => $_POST['password'],
            ];

            if (Validation::sinupValidation($data)) {
                $data['password'] =  password_hash( $data['password'], PASSWORD_DEFAULT);
                $user = new User();


                if ($this->userDao->getUser($user->createUserFromArray($data))) {
                    $error = 'User with this email or username already exists';
                    $this->render('registration.twig', ['warning' => $error]);
                    unset ($error);
                } else {
                    if ($this->userDao->create($user)) {
                        header('Location: /user/login');
                    } else {
                        $error = 'Something went wrong...';
                        $this->render('registration.twig', ['warning' => $error]);
                        unset ($error);
                    }
                }
            } else {
                $error = 'The entered data is invalid';
                $this->render('registration.twig', ['warning' => $error]);
                unset ($error);
            }
        } else {
            if (SessionAuth::isLoggedIn()) {
                header('Location: home');
            } else {
               $this->render('registration.twig');
            }
        }
    }

    public function logoutAction()
    {
        SessionAuth::logOut();
    }

    public function homeAction()
    {
        $posts = array();

        $allPosts = $this->postDao->getAllPosts();

        $userWithId = $this->userDao->getUserbyId($_SESSION['userId']);

        $postsWithImages = Encoder::arrayBlobsToBase64($allPosts, 'post_img');

        foreach ($postsWithImages as $post)
        {
            $id = $post['id'];

            $postLikes = $this->postDao->getTotalLikes($id);


            $post['liked'] = false;

            foreach ($postLikes as $postLike)
            {
                if ($postLike['user_id'] == $_SESSION['userId'])
                {
                    $post['liked'] = true;
                    break;
                }
            }

            $post['likes'] = count($postLikes);

            $posts[] = $post;
        }

        if (SessionAuth::isLoggedIn()) {
            $this->render('home.twig', [
                'firstName' => $userWithId['firstname'],
                'lastName' => $userWithId['lastname'],
                'username' => $userWithId['username'],
                'posts' => $posts,
                'profileimg' => $userWithId['profileimg'],
            ]);
        } else {
            SessionAuth::requireLogIn();
        }
    }

    public function profileAction()
    {
        $posts = $this->postDao->getAllPostsByUserSession($_SESSION);
        $userWithId = $this->userDao->getUserbyId($_SESSION['userId']);

        if (SessionAuth::isLoggedIn()) {
            if (isset($_GET['username']) && $_GET['username'] !== $userWithId['username']) {
                $posts = $this->postDao->getAllPostsByUserName($_GET['username']);
                $user = $this->userDao->getUserByUsername($_GET['username']);

                $posts_with_images = Encoder::arrayBlobsToBase64($posts, 'post_img');

                $this->render('profile.twig', [
                    'firstName' => $user['firstname'],
                    'lastName' => $user['lastname'],
                    'username' => $user['username'],
                    'profileimg' => $user['profileimg'],
                    'postsCount' => count($posts),
                    'posts' => $posts_with_images
                ]);
            } else {

                $posts_with_images = Encoder::arrayBlobsToBase64($posts, 'post_img');
                $this->render('profile.twig', [
                    'firstName' => $userWithId['firstname'],
                    'lastName' => $userWithId['lastname'],
                    'username' => $userWithId['username'],
                    'profileimg' => $userWithId['profileimg'],
                    'postsCount' => count($posts),
                    'posts' => $posts_with_images
                ]);
            }
        } else {
            SessionAuth::requireLogIn();
        }
    }

    public function editAction()
    {
        $userWithId = $this->userDao->getUserbyId($_SESSION['userId']);

        if (SessionAuth::isLoggedIn()) {
            $this->render('edit.twig',[
                'firstName' => $userWithId['firstname'],
                'lastName' => $userWithId['lastname'],
                'username' => $userWithId['username'],
                'profileimg' => $userWithId['profileimg'],
            ]);
        } else {
            SessionAuth::requireLogIn();
        }
    }

    public function searchAction()
    {
        $userWithId = $this->userDao->getUserbyId($_SESSION['userId']);

        if (SessionAuth::isLoggedIn()) {
            $this->render('search.twig',[
                'firstName' => $userWithId['firstname'],
                'lastName' => $userWithId['lastname'],
                'username' => $userWithId['username'],
                'profileimg' => $userWithId['profileimg'],
            ]);
        } else {
            SessionAuth::requireLogIn();
        }
    }

    public function searchQueryAction()
    {
        $toFindUser = new UserDAO();

        $queryResult = $toFindUser->findUser($_POST['search']);

        echo json_encode($queryResult);
    }

    public function chatAction()
    {
        if (SessionAuth::isLoggedIn()) {
            $userWithId = $this->userDao->getUserbyId($_SESSION['userId']);
            $this->render('chat.twig',[
                'firstName' => $userWithId['firstname'],
                'lastName' => $userWithId['lastname'],
                'username' => $userWithId['username']
            ]);
        } else {
            SessionAuth::requireLogIn();
        }
    }

    public function uploadAvatarAction()
    {
        $path = '../resources/avatars/';
        $fname = $_FILES['avatar']['name'];

        if (move_uploaded_file($_FILES['avatar']['tmp_name'], $path .  $_SESSION['userId'] . $fname)) {
            $pathForDB = 'resources/avatars/' . $_SESSION['userId'] . $fname;
            
            $user = new UserDAO();

            $updateUserImg = $user->updateUser($_SESSION['userId'], $pathForDB);

            if ($updateUserImg) {
                $response = [
                    'isAvatarUpdate' => true
                ];

                echo json_encode($response);
            } else {
                $response = [
                    'isAvatarUpdate' => false
                ];

                echo json_encode($response);
            }
        } else {
            $response = [
                'isAvatarUpdate' => false
            ];

            echo json_encode($response);
        }
    }
}



