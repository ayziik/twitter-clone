<?php


namespace App\Controllers;


use App\DAL\PostDAO;
use App\Entity\Post;
use Core\Controller;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class PostController extends Controller
{
    private $postDao;

    public function __construct($route)
    {
        parent::__construct($route);

        $this->postDao = new PostDAO();
        session_start();
    }

    public function createAction()
    {
        !empty($_POST['post_img']) ? $post = new Post($_SESSION['userId'], $_POST['text'], $_POST['post_img'])
                           : $post = new Post($_SESSION['userId'], $_POST['text']);

        $this->postDao->create($post);

        $postFromDB = $this->postDao->getPostByUserSession($_SESSION);


        $postFromDB['post_img'] = base64_encode($postFromDB['post_img']);


        $path = ROOTHPATH . '\Views\user\blocks';
        $loader = new FilesystemLoader($path);
        $twig = new Environment($loader);

        $renderedPost = $twig->render('tweet.twig', [
            'firstName' => $postFromDB['firstname'],
            'lastName' => $postFromDB['lastname'],
            'username' => $postFromDB['username'],
            'profileimg' => $postFromDB['profileimg'],
            'text' => $postFromDB['body'],
            'time' => $postFromDB['created_at'],
            'post_img' => $postFromDB['post_img']
        ]);

        echo json_encode($renderedPost);
    }

    public function deleteAction()
    {
        $post = $_POST['post-id'];


        $this->postDao->delete($post, $_SESSION['userId']);
    }

    public function updateAction()
    {
        $post = $_POST['post-id'];

        $text = $_POST['text'];

        $this->postDao->update($post, $text, $_SESSION['userId']);
    }

    public function likeAction()
    {
        $user_id = $_SESSION['userId'];
        $post_id = json_decode(file_get_contents('php://input'));

        if ($this->postDao->isLike($user_id, $post_id))
        {
            $this->postDao->unlike($user_id, $post_id);

            echo 'unlike';
        } else {
            $this->postDao->like($user_id, $post_id);

            echo 'like';
        }
    }
}