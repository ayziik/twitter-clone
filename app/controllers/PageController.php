<?php


namespace App\Controllers;


use Core\Controller;
use Core\SessionAuth;

class PageController extends Controller
{
    public function indexAction()
    {
        session_start();

        if (SessionAuth::isLoggedIn()) {
            header('Location: user/home');
        } else {
            $this->render('index.twig');
        }
    }

    public function aboutAction()
    {
        echo '<h1>About</h1>';
    }
}