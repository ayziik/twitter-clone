<?php


namespace App\DAL;

use Core\Application;
use App\Entity\User;
use PDO;

class UserDAO
{
    private $db;

    public function __construct()
    {
        $this->db = Application::$db;
    }

    public function create(User $user)
    {
        //query to DB
        $stmt = $this->db->pdo->prepare("INSERT INTO users (firstname, lastname, email, username, dob, password)
                                                VALUES (:firstName, :lastName, :email, :username, :dateOfBirth, :password)");
        $stmt->bindValue(':firstName', $user->getUserFirstName());
        $stmt->bindValue(':lastName', $user->getUserLastName());
        $stmt->bindValue(':email', $user->getUserEmail());
        $stmt->bindValue(':username', $user->getUsername());
        $stmt->bindValue(':dateOfBirth', $user->getUserDateOfBirth());
        $stmt->bindValue(':password', $user->getUserPassword());

        if ($stmt->execute()) {
            return $this->getUser($user);
        } else {
            return false;
        }
    }

    public function getUser(User $user)
    {
        $IsUserExist = $this->db->pdo->prepare('SELECT * FROM users WHERE email = :email OR username = :username LIMIT 1');
        $IsUserExist->bindValue(':email', $user->getUserEmail());
        $IsUserExist->bindValue(':username', $user->getUsername());
        $IsUserExist->execute();

        return $IsUserExist->fetch(PDO::FETCH_ASSOC);
    }

    public function getUserByUsername($username)
    {
        $IsUserExist = $this->db->pdo->prepare('SELECT * FROM users WHERE username = :username LIMIT 1');
        $IsUserExist->bindValue(':username', $username);
        $IsUserExist->execute();

        return $IsUserExist->fetch(PDO::FETCH_ASSOC);
    }

    public function checkUserForLogin(User $user)
    {
        $IsUserExist = $this->db->pdo->prepare('SELECT * FROM users WHERE email = :userLogin OR username = :userLogin LIMIT 1');
        $IsUserExist->bindValue(':userLogin', $user->getUserLogin());
        $IsUserExist->execute();

        $rowFromBase = $IsUserExist->fetch(PDO::FETCH_ASSOC);

        if ($rowFromBase) {
            if (password_verify($user->getUserPassword(), $rowFromBase['password'])) {
                return $rowFromBase;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getUserbyId($id)
    {
        $stmt = $this->db->pdo->prepare('SELECT * FROM users WHERE id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function findUser($searchQuery)
    {
        $search = explode(' ', $searchQuery);
        $count = count($search);
        $arrayForQuery = [];
        $i = 0;

        foreach ($search as $value) {
            $i++;

            if ($i < $count) {
                $arrayForQuery[] = "CONCAT (`firstname`, `lastname`, `username`) LIKE '%" . $value . "%' OR ";
            } else {
                $arrayForQuery[] = "CONCAT (`firstname`, `lastname`, `username`) LIKE '%" . $value . "%'";
            }
        }

        $pdoQuery = "SELECT * FROM users WHERE " . implode("", $arrayForQuery);

        $IsUserExist = $this->db->pdo->query($pdoQuery);

        return $IsUserExist->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateUser($userId, $data)
    {

        $IsUserExist = $this->db->pdo->prepare('UPDATE users SET profileimg = :data  WHERE id = :userId');
        $IsUserExist->bindValue(':userId', $userId);
        $IsUserExist->bindValue(':data', $data);
        $IsUserExist->execute();

        return true;
    }

    public function readAll(array $searchFields)
    {

    }

    public function update(User $user)
    {

    }

    public function delete(User $user)
    {

    }

}