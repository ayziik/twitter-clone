<?php


namespace App\DAL;


use App\Entity\Post;
use Core\Application;
use PDO;

class PostDAO
{
    private $db;

    public function __construct()
    {
        $this->db = Application::$db;
    }

    public function create(Post $post)
    {
        if(is_null($post->getPostImg()))
        {
            $stmt = $this->db->pdo->prepare("INSERT INTO posts (user_id, body)
                                                VALUES (:user_id, :body)");
            $stmt->bindValue(':user_id', $post->getUserId());
            $stmt->bindValue(':body', $post->getBody());
        } else
        {
            $stmt = $this->db->pdo->prepare("INSERT INTO posts (user_id, body, post_img)
                                                VALUES (:user_id, :body, :post_img)");
            $stmt->bindValue(':user_id', $post->getUserId());
            $stmt->bindValue(':body', $post->getBody());

            $pos = strpos($post->getPostImg(), 'base64,');
            $blobData = base64_decode(substr($post->getPostImg(), $pos + 7));

            $stmt->bindValue(':post_img', $blobData, \PDO::PARAM_LOB);
        }

        return $stmt->execute();
    }

    public function read(Post $post)
    {
        // TODO: Implement read() method.
    }

    public function readAll(array $searchFields)
    {
        // TODO: Implement readAll() method.
    }

    public function update($post, $text, $session)
    {
        $stmt = $this->db->pdo->prepare("UPDATE posts SET body = :text WHERE id = :id and user_id = :user");
        $stmt->bindValue(':text', $text);
        $stmt->bindValue(':id', $post);
        $stmt->bindValue(':user', $session['userId']);

        return $stmt->execute();
    }

    public function delete($post, $session)
    {
        $stmt = $this->db->pdo->prepare("DELETE FROM posts WHERE id = :id and user_id = :user");
        $stmt->bindValue(':id', $post);
        $stmt->bindValue(':user', $session['userId']);
        return $stmt->execute();
    }

    public function getAllPosts()
    {
        $stmt = $this->db->pdo->prepare("SELECT firstname, lastname, username, profileimg, body, posts.created_at, posts.post_img, posts.id
                                         FROM posts, users WHERE user_id = users.id ORDER BY created_at DESC");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getPostByUserSession($session){
        $stmt = $this->db->pdo->prepare("SELECT posts.user_id, body, posts.created_at, posts.post_img, 
                                         users.firstname, users.lastname, users.username, users.profileimg 
                                         FROM posts
                                         JOIN users ON posts.user_id = users.id
                                         WHERE users.id = :usersId AND posts.created_at = (SELECT MAX(posts.created_at) FROM posts)");
        $stmt->bindParam(':usersId', $session['userId']);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result;
    }

    public function getAllPostsByUserSession($session)
    {
        $stmt = $this->db->pdo->prepare("SELECT posts.user_id, body, posts.created_at, posts.post_img, users.firstname, users.lastname, users.username 
                                        FROM posts
                                        JOIN users ON posts.user_id = users.id
                                        WHERE users.id = :usersId ORDER BY created_at DESC");
        $stmt->bindParam(':usersId', $session['userId']);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    public function getAllPostsByUserName($username)
    {
        $stmt = $this->db->pdo->prepare("SELECT posts.user_id, body, posts.created_at, posts.post_img, users.firstname, users.lastname, users.username 
                                        FROM posts
                                        JOIN users ON posts.user_id = users.id
                                        WHERE users.username = :username ORDER BY created_at DESC");
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    public function post($body)
    {
    }

    public function getTotalPosts()
    {
    }

    public function getTotalPostsByUserName($username)
    {
    }

    public function like($user_id, $post_id)
    {
        $stmt = $this->db->pdo->prepare("INSERT INTO likes (user_id, post_id)
                                                VALUES (:user_id, :post_id)");
        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':post_id', $post_id);

        return $stmt->execute();
    }

    public function unlike($user_id, $post_id)
    {
        $stmt = $this->db->pdo->prepare("DELETE FROM likes WHERE user_id = :user_id AND post_id = :post_id");
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':post_id', $post_id);

        return $stmt->execute();
    }

    public function isLike($user_id, $post_id)
    {
        $stmt = $this->db->pdo->prepare("SELECT user_id, post_id FROM likes
                                                WHERE user_id = :user_id AND post_id = :post_id");
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':post_id', $post_id);

        $stmt->execute();
        $res = $stmt->fetch();

        return boolval($res);
    }

    public function likesByUserSession($session)
    {
        $stmt = $this->db->pdo->prepare("SELECT * FROM likes
                                                WHERE user_id = :user_id");
        $stmt->bindParam(':user_id', $session['userId']);

        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function getTotalLikes($post_id)
    {
        $stmt = $this->db->pdo->prepare("SELECT post_id, user_id FROM likes
                                                WHERE post_id = :post_id");
        $stmt->bindParam(':post_id', $post_id);

        $stmt->execute();
        $like = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $like;
    }
}
