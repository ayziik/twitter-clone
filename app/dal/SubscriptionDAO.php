<?php


namespace App\DAL;


class SubscriptionDAO
{
    public function create(User $user)
    {
        // TODO: Implement create() method.
    }

    public function getUser(User $user)
    {
        // TODO: Implement read() method.
    }

    public function readAll(array $searchFields)
    {
        // TODO: Implement readAll() method.
    }

    public function update(User $user)
    {
        // TODO: Implement update() method.
    }

    public function delete(User $user)
    {
        // TODO: Implement delete() method.
    }

    public function follow($follower_id, $following_id)
    {
        // TODO: Implement follow() method.
    }

    public function unfollow($follower_id, $following_id)
    {
        // TODO: Implement unfollow() method.
    }

    public function findFollow($follower_id, $following_id)
    {
        // TODO: Implement findFollow() method.
    }

    public function isFollow($follower_id, $following_id)
    {
        // TODO: Implement isFollow() method.
    }

    public function getTotalFollowing()
    {
        // TODO: Implement getTotalFollowing() method.
    }

    public function getTotalFollower()
    {
        // TODO: Implement getTotalFollower() method.
    }

    public function getTotalFollowingByUserName($username)
    {
        // TODO: Implement getTotalFollowingByUserName() method.
    }

    public function getTotalFollowersByUserName($username)
    {
        // TODO: Implement getTotalFollowersByUserName() method.
    }

    public function getFollowingByUserName($username)
    {
        // TODO: Implement getFollowingByUserName() method.
    }

    public function getFollow($id)
    {
        // TODO: Implement getFollow() method.
    }

    public function getFollowersByUserName($username)
    {
        // TODO: Implement getFollowersByUserName() method.
    }
}