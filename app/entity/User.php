<?php


namespace App\Entity;


class User
{
    private $firstName;
    private $lastName;
    private $userLogin;
    private $email;
    private $username;
    private $dateOfBirth;
    private $password;

    public function getUserFirstName()
    {
        return $this->firstName;
    }

    public function getUserLastName()
    {
        return $this->lastName;
    }

    public function getUserLogin()
    {
        return $this->userLogin;
    }

    public function getUserEmail()
    {
        return $this->email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getUserDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function getUserPassword()
    {
        return $this->password;
    }

    public function createUserFromArray(array $data) {
        $this->firstName = $data['firstName'] ?? null;
        $this->lastName = $data['lastName'] ?? null;
        $this->userLogin = $data['userLogin'] ?? null;
        $this->email = $data['email'] ?? null;
        $this->username = $data['username'] ?? null;
        $this->dateOfBirth = $data['dateOfBirth'] ?? null;
        $this->password = $data['password'] ?? null;

        return $this;
    }
}