<?php


namespace App\Entity;


class Post
{
    private $user_id;
    private $body;
    private $post_img;

    public function __construct($user_id, $text, $post_img = null)
    {

        $this->user_id = $user_id;

        $this->body = $text;

        $this->post_img = $post_img;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getPostImg()
    {
        return $this->post_img;
    }
}