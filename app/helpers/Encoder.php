<?php


namespace App\helpers;


class Encoder
{
    static public function arrayBlobsToBase64(array $arr, $key)
    {
        $new_arr = array();
        foreach ($arr as $elem)
        {
            if($elem[$key] !== null)
            {
                $elem[$key] = base64_encode($elem[$key]);
            }
            $new_arr[] = $elem;
        }
        return $new_arr;
    }
}