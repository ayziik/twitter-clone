<?php


namespace App\helpers;


class Validation
{
    public static function firstNameValidation(string $firstName)
    {
        $pattern = '/^[A-Za-zА-ЯЁа-яё]{3,}$/';
        
        return preg_match($pattern, $firstName);
    }

    public static function lastNameValidation(string $lastName)
    {
        $pattern = '/^[A-Za-zА-ЯЁа-яё]{3,}$/';

        return preg_match($pattern, $lastName);
    }

    public static function usernameValidation(string $username)
    {
        $pattern = '/^[A-Za-z]{3,}$/';

        return preg_match($pattern, $username);
    }

    public static function emailValidation(string $email)
    {
        $pattern = '/^([A-Za-z0-9][A-Za-z0-9]*\.?\-?_?)*[A-Za-z0-9]*@([A-Za-z0-9]+([A-Za-z0-9-]*[A-Za-z0-9]+)*\.)+[A-Za-z]{2,}$/';

        return preg_match($pattern, $email);
    }

    public static function passwordValidation(string $password)
    {
        $pattern = '/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/';

        return preg_match($pattern, $password);
    }

    public static function sinupValidation(array $data)
    {
        $validationResult = [];

        $validationResult['firstName'] = self::firstNameValidation($data['firstName']);
        $validationResult['lastName'] = self::lastNameValidation($data['lastName']);
        $validationResult['username'] = self::usernameValidation($data['username']);
        $validationResult['email'] = self::emailValidation($data['email']);
        $validationResult['password'] = self::passwordValidation($data['password']);
        
        foreach ($validationResult as $value) {
            if(!$value) {
                return false;
            }
        }
        
        return true;
    }

    public static function loginValidation(array $data)
    {
        $validationResult = [];

        if (strpos($data['userLogin'], '@')) {
            $validationResult['userLogin'] = self::emailValidation($data['userLogin']);
        } else {
            $validationResult['userLogin'] = self::usernameValidation($data['userLogin']);
        }

        $validationResult['password'] = self::passwordValidation($data['password']);

        foreach ($validationResult as $value) {
            if(!$value) {
                return false;
            }
        }

        return true;
    }
}